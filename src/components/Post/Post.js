import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import axios from '../../axios';
import './Post.css';
import Aux from '../../hoc/Auxi';
import Modal from '../Modal/Modal';
import imageAsset from '../../assets/images/image-not-found.jpg'

class Post extends Component {
    state= {
        hover: false,
        serieDetailHover:{}
    }

    componentDidMount() {
        this.watchForNativeMouseEnter();
        this.watchForNativeMouseLeave();
        if(this.props.id) {
            axios.get('?apikey=bf6aaa52&i=' + this.props.id).then(response => {
                this.setState({serieDetailHover: response.data})
            })
        }
    };

    componentWillUnmount() {
        this.watchForNativeMouseEnter();
        this.watchForNativeMouseLeave();
    }

    watchForNativeMouseEnter() {
        this.refs.hoverElement.addEventListener('mouseenter', () => {
            this.setState({hover: true})
        });
    }

    watchForNativeMouseLeave() {
        this.refs.hoverElement.addEventListener('mouseleave', () => {
            this.setState({hover: false})
        });
    }

    render(){
        const imagePost = this.props.imagePoster !== 'N/A' ? this.props.imagePoster : imageAsset;
        return(
            <article 
                className="Post"
                ref='hoverElement' 
                onClick={this.props.clicked} 
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                id={this.props.id}>
                <Aux>
                    <div className="Image">
                        <img src={imagePost}/>
                    </div>
                    <h3>{this.props.title}</h3>
                    <Modal show={this.state.hover}>
                        <h2>{this.state.serieDetailHover.Title}</h2>
                        <p>Actors: {this.state.serieDetailHover.Actors}</p>
                        <p>Rating: {this.state.serieDetailHover.imdbRating}</p>
                    </Modal>
                </Aux>
            </article>
        ) 
    }
}

export default withRouter(Post);