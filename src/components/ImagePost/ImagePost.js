import React from 'react';

import imageAsset from '../../assets/images/image-not-found.jpg'

const imagePost = (props) => {
    const image = props.imageSrc ? props.imageSrc : imageAsset;
    <div className="Right-content">
        <img src={image}/>
        <p>{props.imageTitle}</p>
    </div>
}

export default imagePost;

