import React, { Component } from 'react';
import axios from '../../../axios';

import './FullPost.css';
import ImagePost from '../../../components/ImagePost/ImagePost';

class FullPost extends Component {
    state = {
        loadedPost: null,
        seasons:[],
        episodes:[],
        season: null,
        activeSeason: null,
        activeEpisode:null,
        episode:null
    }

    componentDidMount(){
        this.loadData();
    }

    loadData(){
        if(this.props.match.params.id) {
            if(!this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== +this.props.match.params.id)) {
                axios.get('?apikey=bf6aaa52&i=' + this.props.match.params.id).then(response => {
                    const updateSeasonsList = Array.apply(null, {length: response.data.totalSeasons}).map(Number.call, Number);

                    this.setState({
                        loadedPost: response.data, 
                        seasons:[...this.state.seasons, ...updateSeasonsList]}) 
                })
            }
        }
    }

    onClickSeasonhandler = (idSeason) => {
        const idRec = idSeason;
        this.setState({season: idSeason, activeSeason: idSeason-1});
        if(idRec != this.state.season){
            this.setState({episodes: []});
        }
        axios.get('?apikey=bf6aaa52&i=' + this.props.match.params.id +'&Season=' + idSeason).then(response => {
            this.setState({episodes: [
                ...this.state.episodes,
                ...response.data.Episodes
            ]})
            //console.log(this.state.episodes)
            //this.setState({episodes: updateEpisodes})
        })
    }

    onClickEpisodeHandler = (idEpisode, index) => {
        axios.get('?apikey=bf6aaa52&i=' + this.props.match.params.id +'&Season=' + this.state.season + '&episode=' + idEpisode).then(response => {
            this.setState({episode: response.data, activeEpisode: index})
        })
    }

    render () {
        let post = <p style={{textAlign: 'center'}}>Please select a Post!</p>;
        let episode='';
        if(this.props.match.params.id) {
            post = <p style={{textAlign: 'center'}}>Loading...!</p>;
        }
        if(this.state.episode) {
            const rating = ((this.state.episode.imdbRating * 100) / 10).toFixed(0);
            episode = (
                <div className="episode-content">
                    <p>{this.state.episode.Released}</p>
                    <p>{this.state.episode.Plot}</p>
                    <p>{rating}%</p>
                </div>
            )
        }
        if(this.state.loadedPost) {
            post = (
                <div className="FullPost">
                    {/* <ImagePost 
                        imageSrc={this.state.loadedPost.Poster}
                        imageTitle={this.state.loadedPost.Title}/> */}
                    
                    <div className="left-content">
                        <h1>Seasons</h1>
                        <ul className="season-list">
                            {this.state.seasons.slice(0, 10).map((list, i) => {
                                return <li 
                                    key={list} 
                                    className={ this.state.activeSeason === i ? 'active' : null } 
                                    onClick={() => this.onClickSeasonhandler(list+1)} >{list+1}</li>
                            })}
                        </ul>
                        <ul className="episode-list">
                            {this.state.episodes.map((epsConatiner, index) => {
                                return <li 
                                    key={epsConatiner.imdbID}
                                    className={ this.state.activeEpisode === index ? 'active' : null } 
                                    onClick={() => this.onClickEpisodeHandler(epsConatiner.Episode, index)}>{index + 1} <span className="episode-title">{epsConatiner.Title}</span>
                                    <span className="icon-episode"></span>
                                        {episode}
                                    </li>
                            })}
                        </ul>
                    </div>
                </div>
    
            );
        } 
        return post;
    }
}

export default FullPost;