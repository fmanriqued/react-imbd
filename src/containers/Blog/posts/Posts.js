import React, { Component } from 'react';

import Post from '../../../components/Post/Post';
import './Posts.css';


class Posts extends Component {
    state = {
        responseMessage: 'Please search any serie',
        activeId: null
    }

    postSelectedHandler = (id) => {
        this.props.history.push('/posts/' + id);
    }

    render() {
        let posts = <p style={{textAlign: 'center', color: 'red'}}>{this.state.responseMessage}</p>;
        if (this.props.Posts && this.props.Posts.length) {
            posts = this.props.Posts.map(post=>{
                if(post.imdbID) {
                    return (
                        <Post
                            key={post.imdbID}
                            id={post.imdbID}      
                            title={post.Title} 
                            imagePoster={post.Poster}
                            clicked={() => this.postSelectedHandler(post.imdbID)}/>
                    )
                }
            })
        }

        return(
            <section className="Posts">
                {posts}
            </section>
        )
    }
}

export default Posts;