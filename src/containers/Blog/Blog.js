import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import axios from '../../axios';

import './Blog.css';
import Posts from './posts/Posts';
import Header from '../Header/Header';
import FullPost from './FullPost/FullPost';
import Aux from '../../hoc/Auxi';

class Blog extends Component {
    state = {
        posts:[],
        respSearch: false,
        page: 1,
        searchSerie: null,
        enableFinder: false
    }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll, false);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll, false);
    }
    

    onSearchHandler = (event) => {
        console.log(event.target.value.length)
        if(event.target.value.length) {
            const regex = new RegExp("^[0-9a-zA-Z\b ]+$");
            if (regex.exec(event.target.value) == null) {
               console.log('error', event.currentTarget.value)
               return false;
            }

            this.setState({searchSerie: event.target.value});
            
            axios.get('?s=' + event.target.value + '&page='+this.state.page+'&type=series&apikey=bf6aaa52')
            .then(response => {
                this.setState({
                    posts: [this.state.posts, ...response.data.Search], 
                    respSearch: true})    
            })
            .catch(error => {
                this.setState({posts: [], respSearch: false}) 
            });

            //const iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
            /* for (var i = 0; i < event.target.value.length; i++) {
                if (iChars.indexOf(event.target.value.charAt(i)) != -1) {
                    alert ("You are using special characters. \nThese are not allowed.\n Please remove them and try again.");
                    return false;
                }else {
                    this.setState({searchSerie: event.target.value});
                    axios.get('?s=' + event.target.value + '&page='+this.state.page+'&type=series&apikey=bf6aaa52')
                    .then(response => {
                        this.setState({
                            posts: [this.state.posts, ...response.data.Search], 
                            respSearch: true})    
                    })
                    .catch(error => {
                        this.setState({posts: [], respSearch: false}) 
                    });
                }
            } */
        }else {
            this.setState({posts: [], respSearch: false})   
        }
    }

    onScroll = () => {
        if ((window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) && this.state.posts) {   
            this.setState({page: this.state.page+1}) 
            axios.get('?s=' + this.state.searchSerie + '&page='+this.state.page+'&type=series&apikey=bf6aaa52')
            .then(response => {
                const newPosts = response.data.Search;
                const updatedPosts = newPosts.map(post=> {
                    return post
                })
                let newUpdateArr = this.state.posts.concat(updatedPosts);
                this.setState({posts: newUpdateArr, respSearch: true})   
            })
            .catch(error => {
                // this.setState({error: true})
            }); 
        }
    }

    onSearchClickHandler = () => {
        this.setState(( prevState ) => { return { enableFinder: !prevState.enableFinder} })
    }

    render () {
        return (
            <Aux>
                <Header 
                    changed={this.onSearchHandler} 
                    enabledFinder={this.onSearchClickHandler} 
                    enabled={this.state.enableFinder}/>
                <Switch> 
                    <Route  path= "/posts/:id" exact component={FullPost} /> 
                    <Route  path="/" render={(props) => <Posts {...props} Posts={this.state.posts} RespSearch={this.state.respSearch}/>} />
                    <Route render={() => <h1>Not found</h1>} />
                </Switch>
            </Aux>
        );
    }
}

export default Blog;