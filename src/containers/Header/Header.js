import React from 'react';
import { Link } from 'react-router-dom';

import Aux from '../../hoc/Auxi';
import './Header.css';

const Header = (props) => {
    let linkGoBack = '';

    if(window.location.pathname.split('/')[1] === 'posts'){
        linkGoBack = <Link to="/" className="button prev">back</Link>
    }

    return (
        <Aux>
            <div className="Header">
                <div className="Nav">
                    {linkGoBack}
                    <h2>Shows</h2>
                </div>
                <div className="search-input">
                    <i className="fa fa-search" onClick={props.enabledFinder}></i>
                    <input type="text" 
                        onKeyUp={props.changed} 
                        placeholder="Please write a serie" 
                        hidden={!props.enabled}
                        pattern="^[a-zA-Z0-9]+$"/>
                </div>
            </div>
        </Aux>
    )
}

export default Header;